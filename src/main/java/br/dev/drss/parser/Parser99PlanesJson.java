package br.dev.drss.parser;

import br.dev.drss.model.PlanoVoo99Planes;

public class Parser99PlanesJson  extends ParseFileJson<PlanoVoo99Planes> {
	
	@Override
	public String nameFile() {
		return "99planes.json";
	}

}
