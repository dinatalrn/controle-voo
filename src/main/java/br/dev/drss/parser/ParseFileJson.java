package br.dev.drss.parser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class ParseFileJson<T> extends ParseFile<T> {

	@Override
	public List<T> parseToList( Class<T> contentClass ) {
		List<T> listOfObjects = null;
		if( this.isValidFilename() ) {
			ObjectMapper mapper = new ObjectMapper();
			JavaType typeReference = mapper.getTypeFactory().constructParametricType(List.class, contentClass);
			try {
				listOfObjects = mapper.readValue(new File(pathToResourceFile()), typeReference);
			} catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
		return listOfObjects;
	}

	@Override
	public abstract String nameFile();

}
