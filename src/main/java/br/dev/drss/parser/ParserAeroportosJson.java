package br.dev.drss.parser;

import br.dev.drss.model.Aeroporto;

public class ParserAeroportosJson  extends ParseFileJson<Aeroporto> {
	
	@Override
	public String nameFile() {
		return "aeroportos.json";
	}

}
