package br.dev.drss.parser;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

public abstract class ParseFile<T> {

	public String pathToResourceFile() {
		if( this.isValidFilename() ) {
			try {
				return new ClassPathResource( this.nameFile() ).getURI().getPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public boolean isValidFilename() {
		String filename = this.nameFile();
		return ( filename != null && !filename.isEmpty() );
	}
	
	public abstract List<T> parseToList( Class<T> contentClass );
	
	public abstract String nameFile();

}
