package br.dev.drss.parser;

import java.io.File;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import br.dev.drss.model.PlanoVooUberAir;

public class ParserUberairCsv extends ParseFile<PlanoVooUberAir> {

	public List<PlanoVooUberAir> parseToList(Class<PlanoVooUberAir> contentClass) {
		List<PlanoVooUberAir> voos = null;
		
		try {
			CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
			CsvMapper mapper = new CsvMapper();
			File file = new File(pathToResourceFile());
			MappingIterator<PlanoVooUberAir> readValues = mapper.readerFor(contentClass).with(bootstrapSchema).readValues(file);
			voos = readValues.readAll();
		} catch (Exception e) {
			e.printStackTrace();
			voos = Collections.emptyList();
		}
		
		return voos;
	}

	@Override
	public String nameFile() {
		return "uberair.csv";
	}
	
}
