package br.dev.drss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaVooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaVooApplication.class, args);
	}

}
