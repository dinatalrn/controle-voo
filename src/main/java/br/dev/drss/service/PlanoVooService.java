package br.dev.drss.service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.dev.drss.dto.PlanoVooDTO;
import br.dev.drss.model.Escala;
import br.dev.drss.model.PlanoVoo;
import br.dev.drss.model.PlanoVoo99Planes;
import br.dev.drss.model.PlanoVooUberAir;
import br.dev.drss.parser.ParseFile;
import br.dev.drss.parser.ParseFileJson;
import br.dev.drss.parser.Parser99PlanesJson;
import br.dev.drss.parser.ParserUberairCsv;

public class PlanoVooService {
    
	public static List<Escala> busca(String origem, String destino, String data) {
		
		ParseFile<PlanoVooUberAir> pfUberAir = new ParserUberairCsv();
		ParseFileJson<PlanoVoo99Planes> pf99Planes = new Parser99PlanesJson();
		
		List<PlanoVooUberAir> voosUberAir = pfUberAir.parseToList(PlanoVooUberAir.class);
		List<PlanoVoo99Planes> voos99Planes = pf99Planes.parseToList(PlanoVoo99Planes.class);
		
//		Stream<PlanoVoo> combinedStream = Stream.of(voosUberAir, voos99Planes).flatMap(Collection::stream);
		Stream<PlanoVoo> listasCombinadasStream = Stream.concat(voosUberAir.stream(), voos99Planes.stream());
		List<PlanoVooDTO> todosVoos = listasCombinadasStream
			.map(new Function<PlanoVoo, PlanoVooDTO>(){
				@Override
				public PlanoVooDTO apply(PlanoVoo t) {
					return new PlanoVooDTO(t);
				}		
			}).collect(Collectors.toList());
		
		List<Escala> escalas = EscalaService.buscarVoos(origem, destino, data, todosVoos);
		
		return escalas;
	}
	
}
