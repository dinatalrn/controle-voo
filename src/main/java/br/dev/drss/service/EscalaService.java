package br.dev.drss.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import br.dev.drss.dto.PlanoVooDTO;
import br.dev.drss.model.Escala;
import br.dev.drss.util.CalendarUtil;

public class EscalaService {
	
	public static List<Escala> buscarVoos(String origem, String destino, String data, List<PlanoVooDTO> voos){
		List<Escala> escalas = new ArrayList<Escala>();
		Date dataSaida = CalendarUtil.stringToDate(data);
		
		// Inicializa possiveis voos
		escalas.addAll( 
			voos.stream()
				.filter( plano -> plano.matchOrigemAndDataSaida(origem, dataSaida) )
				.map( new Function<PlanoVooDTO, Escala>() {

					@Override
					public Escala apply(PlanoVooDTO pv) {
						return new Escala(pv);
					}
				})
				.collect(Collectors.toList())
		);
		
		escalas = procuraTrechosAteDestinoInEscalas(voos, escalas, destino);

		Collections.sort(escalas);
		return escalas;
	}

	private static List<Escala> procuraTrechosAteDestinoInEscalas(List<PlanoVooDTO>todosVoos, List<Escala> escalas, String destino) {
		
		for (int i=0; i<escalas.size(); i++) {
			procuraTrechosAteDestino(todosVoos, escalas.get(i), destino);
		}
		
		return escalas.stream()
				.filter( e -> e.getDestino().equals(destino) )
				.collect(Collectors.toList());
		
	}

	private static void procuraTrechosAteDestino(List<PlanoVooDTO> todosVoos, Escala escala, String destino) {
		int numTrechos = escala.getTrechos().size();
		if( numTrechos > 0 ) {
			
			PlanoVooDTO ultimoTrecho = escala.getTrechos().get(numTrechos-1);
			if( !ultimoTrecho.getDestino().equals(destino) ) {
				
				List<String> destinos = escala.getTrechos().stream().map( new Function<PlanoVooDTO, String>() {
					@Override
					public String apply(PlanoVooDTO t) {
						return t.getDestino();
					} 
				}).collect(Collectors.toList());
				destinos.add( escala.getTrechos().get(0).getOrigem() );
				
				List<PlanoVooDTO> conexoes = todosVoos.stream()
						.filter(plano -> isConexaoAndDatasValidasAndHorasConexaoAndNovoDestino(ultimoTrecho, plano, 12, destinos))
						.collect(Collectors.toList());
				
				for (Iterator<PlanoVooDTO> iterator = conexoes.iterator(); iterator.hasNext();) {
					PlanoVooDTO conexao = (PlanoVooDTO) iterator.next();
					boolean isDestinoFinal = conexao.getDestino().equals(destino);
					/**
					 * Verifica se é o destino final ou se a viagem dura por no máximo 48h.
					 */
					if( isDestinoFinal || CalendarUtil.diffDatesIsInsideHours(escala.getSaida(), conexao.getChegada(), 48) ) {
						escala.addTrecho(conexao);
						if( isDestinoFinal ) {
							break;
						}
						procuraTrechosAteDestino(todosVoos, escala, destino);
						if( !escala.getDestino().equals(destino) ) {
							escala.removeUltimoTrecho();
						}
					}
				}
				
			}
		}
	}
	
	private static boolean isConexaoAndDatasValidasAndHorasConexaoAndNovoDestino(PlanoVooDTO ultimoTrecho, PlanoVooDTO plano, int hours, List<String> destinos) {
		Date ini = ultimoTrecho.getChegada();
		Date end = plano.getSaida();
		boolean isConexao = ultimoTrecho.getDestino().equals(plano.getOrigem());
		boolean tempoHabil = isConexao && ini.compareTo(end) < 0 &&  CalendarUtil.diffDatesIsInsideHours(ini, end, hours);
		return ( isConexao && tempoHabil && (!destinos.contains(plano.getDestino())) );
	}
	
}
