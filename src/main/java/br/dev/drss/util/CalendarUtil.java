package br.dev.drss.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarUtil {

    private static SimpleDateFormat sdfDataHora = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static SimpleDateFormat sdfData = new SimpleDateFormat("yyyy-MM-dd");

    public static Date convertStringDateEnUsToDate(String strEnUs){
    	return convertStringDateEnUsToDate(strEnUs, "00:00");
	}

    public static Date convertStringDateEnUsToDate(String strEnUs, String hour){
    	if( strEnUs.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}") ) {
    		try {
				return sdfDataHora.parse(strEnUs + " " + hour);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	return null;
    }
    
    public static Date addDays(Date date, int days) {
		if( days < 1 )
			return date;
    	Calendar c = GregorianCalendar.getInstance();
    	c.setTime(date);
    	c.add(1, Calendar.DATE);
    	return c.getTime();
	}
    
    public static Date stringToDate(String strDate) {
    	try {
			return sdfData.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    public static boolean diffDatesIsInsideHours(Date ini, Date end, int hours) {
    	long diff = end.getTime() - ini.getTime();
    	long diffMinutes = diff / (60 * 1000); 
    	long hoursInMinutes = hours * 60;
    	return diffMinutes < hoursInMinutes;
	}
	
}
