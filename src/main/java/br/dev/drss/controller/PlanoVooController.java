package br.dev.drss.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.dev.drss.model.Aeroporto;
import br.dev.drss.model.Escala;
import br.dev.drss.parser.ParseFileJson;
import br.dev.drss.parser.ParserAeroportosJson;
import br.dev.drss.service.PlanoVooService;

@RestController
public class PlanoVooController {

	@PostMapping("/consulta")
	public List<Escala> consulta(
			@RequestParam String origem,
			@RequestParam String destino,
			@RequestParam String data ) {
		List<Escala> voos = PlanoVooService.busca(origem, destino, data);
		return voos;
	}

	@RequestMapping("/aeroportos")
	public List<Aeroporto> aeroportos() {
		ParseFileJson<Aeroporto> pfJson = new ParserAeroportosJson();
		List<Aeroporto> aeroportos = pfJson.parseToList(Aeroporto.class);
		return aeroportos;
	}
	
}
