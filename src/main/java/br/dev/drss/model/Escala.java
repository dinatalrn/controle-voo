package br.dev.drss.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.dev.drss.dto.PlanoVooDTO;

public class Escala implements Comparable<Escala>{
    
    private String origem;

    private String destino;
    
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.sss'Z'")
    private Date saida;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.sss'Z'")
    private Date chegada;
    
    List<PlanoVooDTO> trechos;
    	
	public Escala(PlanoVooDTO planoVooDTO) {
		this.origem = planoVooDTO.getOrigem();
		this.destino = planoVooDTO.getDestino();
		this.saida = planoVooDTO.getSaida();
		this.chegada = planoVooDTO.getChegada();
		this.trechos = new ArrayList<PlanoVooDTO>();
		this.trechos.add(planoVooDTO);
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Date getSaida() {
		return saida;
	}

	public void setSaida(Date saida) {
		this.saida = saida;
	}

	public Date getChegada() {
		return chegada;
	}

	public void setChegada(Date chegada) {
		this.chegada = chegada;
	}

	public List<PlanoVooDTO> getTrechos() {
		return trechos;
	}

	public void setTrechos(List<PlanoVooDTO> trechos) {
		this.trechos = trechos;
	}

	@Override
	public int compareTo(Escala o) {
		return this.getSaida().compareTo(o.getSaida());
	}
	
	public void addTrecho(PlanoVooDTO planoVooDTO) {
		this.trechos.add(planoVooDTO);
		this.destino = planoVooDTO.getDestino();
		this.chegada = planoVooDTO.getChegada();
	}
	
	public void removeUltimoTrecho() {
		int countTrechos = this.trechos.size();
		if( countTrechos > 1 ) {
			this.trechos.remove(countTrechos-1);
			this.chegada = this.trechos.get(countTrechos-2).getChegada();
			this.destino = this.trechos.get(countTrechos-2).getDestino();
		}
	}
	
}
