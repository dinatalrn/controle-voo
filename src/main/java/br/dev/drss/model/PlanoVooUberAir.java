package br.dev.drss.model;

public class PlanoVooUberAir extends PlanoVoo{
	
	private String operadora = "UberAir";

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

}
