package br.dev.drss.model;

public class PlanoVoo99Planes extends PlanoVoo {
	
	private String operadora = "99Planes";

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

}
