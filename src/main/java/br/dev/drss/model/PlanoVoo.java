package br.dev.drss.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PlanoVoo {

	@JsonProperty
	@JsonAlias({"numero_voo"})
    private String voo;
    
	@JsonProperty
	@JsonAlias({"aeroporto_origem"})
    private String origem;

	@JsonProperty
	@JsonAlias({"aeroporto_destino"})
    private String destino;
    
    @JsonProperty(value="data_saida")
    @JsonAlias({"data"})
    private String dataSaida;

	@JsonProperty
	@JsonAlias({"horario_saida"})
    private String saida;

	@JsonProperty
	@JsonAlias({"horario_chegada"})
    private String chegada;

	@JsonProperty
	@JsonAlias({"preco"})
    private BigDecimal valor;
    
    private String operadora;

	public String getVoo() {
		return voo;
	}

	public void setVoo(String voo) {
		this.voo = voo;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}

	public String getSaida() {
		return saida;
	}

	public void setSaida(String saida) {
		this.saida = saida;
	}

	public String getChegada() {
		return chegada;
	}

	public void setChegada(String chegada) {
		this.chegada = chegada;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}
	
	public boolean matchOrigemAndDataSaida(String origem, String dataSaida) {
		boolean matchResult = this.getOrigem().equals(origem);
		if( matchResult ) {
			return (this.getDataSaida().equals(dataSaida));
		}
		return matchResult;
	}
     
}
