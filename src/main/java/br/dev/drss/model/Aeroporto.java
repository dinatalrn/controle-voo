package br.dev.drss.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Aeroporto {

	@JsonProperty(value="aeroporto")
	private String codigo;
	
	private String nome;
	
	private String cidade;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
}
