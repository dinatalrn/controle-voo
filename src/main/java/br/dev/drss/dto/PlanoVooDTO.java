package br.dev.drss.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import br.dev.drss.model.PlanoVoo;
import br.dev.drss.util.CalendarUtil;

public class PlanoVooDTO {

	@JsonIgnore
    private String voo;
    
    private String origem;

    private String destino;
    
    @JsonIgnore
    private Date dataSaida;
    
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.sss'Z'")
    private Date saida;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.sss'Z'")
    private Date chegada;
    
    private BigDecimal valor;
    
    private String operadora;
    
    public PlanoVooDTO(PlanoVoo planoVoo) {
		this.voo = planoVoo.getVoo();
		this.origem = planoVoo.getOrigem();
		this.destino = planoVoo.getDestino();
		this.operadora = planoVoo.getOperadora();
		this.valor = planoVoo.getValor();
		
		this.dataSaida = CalendarUtil.stringToDate(planoVoo.getDataSaida());
		this.saida = CalendarUtil.convertStringDateEnUsToDate(planoVoo.getDataSaida(), planoVoo.getSaida());
		this.chegada = CalendarUtil.convertStringDateEnUsToDate(planoVoo.getDataSaida(), planoVoo.getChegada());
		if( this.chegada !=null && this.chegada.compareTo(this.getSaida()) < 0 ) {
	    	CalendarUtil.addDays(this.chegada, 1);
	    }
	}

	public String getVoo() {
		return voo;
	}

	public void setVoo(String voo) {
		this.voo = voo;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getSaida() {
		return saida;
	}

	public void setSaida(Date saida) {
		this.saida = saida;
	}

	public Date getChegada() {
		return chegada;
	}

	public void setChegada(Date chegada) {
		this.chegada = chegada;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}
	
	public boolean matchOrigemAndDataSaida(String origem, Date dataSaida) {
		boolean matchResult = this.getOrigem().equals(origem);
		if( matchResult ) {
			return (this.getDataSaida().compareTo(dataSaida) == 0);
		}
		return matchResult;
	}
   
}