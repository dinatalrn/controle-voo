# Controle de Vôo

Controle de Vôo é um projeto de aplicação do Spring Boot.

## Utilização

Execute o seguinte comando na raiz do projeto.

```bash
./mvnw spring-boot:run
```

## Descrição
A aplicação possui 2 endpoints:

### GET  /aeroportos
Requisição sem parâmetros, apenas lista os aeroportos disponíveis.

### POST  /consulta
Requisição com parâmetros obrigatórios: origem(sigla aeroporto), destino(sigla aeroporto) e data(data de saída). Observe que as escalas são determinadas em até 48h após a saída no aeroporto de origem, ou caso passe das 48h e seja o destino final da viagem consultada.

## Licença
[MIT](https://choosealicense.com/licenses/mit/)
